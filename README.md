# API Server (Docker Compose)

# Installation
```bash
git clone git@gitlab.com:connectique-ndi-2021/api-server-docker-compose.git /docker/ndi2021-api-server
cd /docker/ndi2021-api-server
sh init.sh
```